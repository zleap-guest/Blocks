# Blocks
Blocks - Block based Python Programming ( uses Trinket.io)
This uses [Trinket.io](https://trinket.io/),  which is the website [Codeclub](https://codeclub.org/en/) use for Python and HTML/CSS projects. 

Getting started,  once you have navigated to the Trinket website, either login, create an account or if you are not bothered about saving you work just go ahead and create a Blocks project.

![Navigate to the blocks coding section](https://github.com/zleap/Blocks/blob/master/trinket1.png)


If you don't have a login then you need to click on Learn and you can find a link to Blocks from there. 

We are going to use Blocks to create a very simple nested loop pattern using Turtle Graphics

![output](https://github.com/zleap/Blocks/blob/master/blocks-result.png)

Lets start with the code that produces the above. 

![block1](https://github.com/zleap/Blocks/blob/master/blocks.png)

Trinket will allow us to examine the Python code behind this:

![python-code-for-above](https://github.com/zleap/Blocks/blob/master/blocks-code.png)

Which is really handy as we want to learn how this actually works.

![add some more code ](https://github.com/zleap/Blocks/blob/master/blocks2.png)

I have added some extra blocks to help tidy things up
1. start with pen done
2. pen up at the end
3. add a message to print "done" when finished.

--

This how_to has been created by Paul Sutton (zleap) [http://www.zleap.net](http://www.zleap.net)

Please feel free to modify and  improve,  pull requests welcome.  All graphics (other then the cc logo below) have been created, by myself with screenshots from Trinket.io

Pull request back are welcome. 
Hope this resource is useful.

![cc-logo](https://github.com/zleap/Blocks/blob/master/88x31.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbODc5NzkxNTA3XX0=
-->